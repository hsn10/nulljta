Null JTA provider
=================

This is null JTA provider. It is supposed to used by software requiring
JTA, but not using it for anything beyond local transactions. In such
cases installing full sized JTA like JBOSS TM is overkill.

While it does nothing, it is still valid JTA transaction manager and
catches illegal invocation of JTA methods from calling applications
and invokes registered callbacks.

NullJTA has 100% code coverage junit test suite and passes J2EE JTA 1.1
validation suite.

Radim Kolar