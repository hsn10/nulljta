/**
 *    Copyright 2013 Radim Kolar
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.filez.jta.nulljta;

import static org.mockito.Mockito.*;

import javax.naming.NamingException;
import javax.naming.Reference;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;

import org.junit.Before;
import org.junit.Test;

import com.carrotsearch.randomizedtesting.RandomizedContext;
import com.carrotsearch.randomizedtesting.RandomizedTest;

public class TestUserTransaction extends RandomizedTest {

	private NullTransactionManager manager;
	private NullUserTransaction utx;

	@Before
	public void setUp()  {
		manager = mock(NullTransactionManager.class);
		utx = new NullUserTransaction(manager);
	}

	@SuppressWarnings("static-method")
	@Test(expected=IllegalArgumentException.class)
	public void testNullUserTransaction() {
		assertNull(new NullUserTransaction(null));
	}

	@Test
	public void testUserTransaction() {
		assertNotNull(new NullUserTransaction(manager));
		verifyZeroInteractions(manager);
	}

	@Test
	public void testBegin() throws NotSupportedException, SystemException {
		utx.begin();
		verify(manager).begin();
	}

	@Test
	public void testCommit() throws SecurityException, IllegalStateException, RollbackException, HeuristicMixedException, HeuristicRollbackException, SystemException {
		utx.commit();
		verify(manager).commit();
	}

	@Test
	public void testRollback() throws IllegalStateException, SecurityException, SystemException {
		utx.rollback();
		verify(manager).rollback();
	}

	@Test
	public void testSetRollbackOnly() throws IllegalStateException, SystemException {
		utx.setRollbackOnly();
		verify(manager).setRollbackOnly();
	}

	@Test
	public void testGetStatus() throws SystemException {
		utx.getStatus();
		verify(manager).getStatus();
	}

	@Test
	public void testSetTransactionTimeout() throws SystemException {
		RandomizedContext context = RandomizedContext.current();
		int timeout = context.getRandom().nextInt();
		utx.setTransactionTimeout(timeout);
		verify(manager).setTransactionTimeout(timeout);
	}

	@Test
	public void testGetReference() throws NamingException {
		assertEquals(new Reference(NullUserTransaction.class.getName()), utx.getReference());
	}
}
