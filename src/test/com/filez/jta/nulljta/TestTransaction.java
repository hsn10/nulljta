/**
 *    Copyright 2013 Radim Kolar
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.filez.jta.nulljta;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.Synchronization;
import javax.transaction.SystemException;
import javax.transaction.Transaction;

import org.junit.Before;
import org.junit.Test;

public class TestTransaction {

	private Transaction trans;
	private Synchronization sync;

	@Before
	public void setUp() {
		trans = new NullTransaction();
		sync = mock(Synchronization.class);
	}

	@Test
	public void testCommit() throws SecurityException, RollbackException, HeuristicMixedException, HeuristicRollbackException, SystemException {
		trans.commit();
		assertEquals(Status.STATUS_COMMITTED, trans.getStatus());
	}

	@Test(expected=IllegalStateException.class)
	public void testCommitTwice() throws SecurityException, RollbackException, HeuristicMixedException, HeuristicRollbackException, SystemException {
		trans.commit();
		trans.commit();
	}

	@Test
	public void testRollback() throws IllegalStateException, SystemException {
		trans.rollback();
		assertEquals(Status.STATUS_ROLLEDBACK, trans.getStatus());
	}

	@Test(expected=IllegalStateException.class)
	public void testRollbackTwice() throws IllegalStateException, SystemException {
		trans.rollback();
		trans.rollback();
	}

	@Test(expected=RollbackException.class)
	public void testSetRollbackOnlyAndCommit() throws SecurityException, RollbackException, HeuristicMixedException, HeuristicRollbackException, SystemException {
		trans.setRollbackOnly();
		trans.commit();
	}

	@Test
	public void testSetRollbackOnly() throws SecurityException, SystemException {
		trans.setRollbackOnly();
		assertEquals(Status.STATUS_MARKED_ROLLBACK, trans.getStatus());
	}

	@Test(expected=IllegalStateException.class)
	public void testSetRollbackOnlyAfterCommit() throws SecurityException, SystemException, RollbackException, HeuristicMixedException, HeuristicRollbackException {
		trans.commit();
		trans.setRollbackOnly();
	}

	@Test
	public void testSetRollbackOnlyAndRollback() throws SecurityException, SystemException {
		trans.setRollbackOnly();
		trans.rollback();
	}

	/** initial status should be ACTIVE */
	@Test
	public void testGetStatus() throws SystemException {
		assertEquals(Status.STATUS_ACTIVE, trans.getStatus());
	}

	/* should be able to enlist everything */
	@Test
	public void testEnlistResource() throws IllegalStateException, RollbackException, SystemException {
		assertTrue(trans.enlistResource(null));
	}

	@Test
	public void testDelistResource() throws IllegalStateException, SystemException {
		assertTrue(trans.delistResource(null, 0));
	}

	@Test
	public void testRegisterSynchronization() throws IllegalStateException, RollbackException, SystemException {
		trans.registerSynchronization(sync);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testRegisterNullSynchronization() throws IllegalStateException, RollbackException, SystemException {
		trans.registerSynchronization(null);
	}

	@Test
	public void testRegisterSynchronizationAfterCommit() throws IllegalStateException, RollbackException, SystemException, SecurityException, HeuristicMixedException, HeuristicRollbackException {
		trans.commit();
		trans.registerSynchronization(sync);
		verify(sync, times(1)).afterCompletion(Status.STATUS_COMMITTED);
	}

	@Test
	public void testRegisterSynchronizationAfterRollback() throws IllegalStateException, RollbackException, SystemException, SecurityException {
		trans.rollback();
		trans.registerSynchronization(sync);
		verify(sync, times(1)).afterCompletion(Status.STATUS_ROLLEDBACK);
	}

	@Test(expected=RollbackException.class)
	public void testRegisterSynchronizationForRollbackOnly() throws IllegalStateException, RollbackException, SystemException, SecurityException {
		trans.setRollbackOnly();
		trans.registerSynchronization(sync);
	}

	@Test
	public void testSynchronizationCalledAfterRollback() throws IllegalStateException, RollbackException, SystemException, SecurityException {
		trans.registerSynchronization(sync);
		trans.rollback();
		verify(sync, times(1)).afterCompletion(Status.STATUS_ROLLEDBACK);
		verify(sync, never()).beforeCompletion();
	}

	@Test
	public void testSynchronizationCalledAfterCommit() throws IllegalStateException, RollbackException, SystemException, SecurityException, HeuristicMixedException, HeuristicRollbackException {
		trans.registerSynchronization(sync);
		trans.commit();
		verify(sync, times(1)).beforeCompletion();
		verify(sync, times(1)).afterCompletion(Status.STATUS_COMMITTED);
	}
}
