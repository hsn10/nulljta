/**
 *    Copyright 2013 Radim Kolar
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.filez.jta.nulljta;

import static org.junit.Assert.*;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.InvalidTransactionException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.Transaction;

import org.junit.Before;
import org.junit.Test;

public class TestTransactionManager {

	private NullTransactionManager manager;

	@Before
	public void setUp() {
		manager = new NullTransactionManager();
	}

	@Test
	public void testBegin() throws NotSupportedException, SystemException {
		manager.begin();
	}

	@Test(expected=NotSupportedException.class)
	public void testBeginTwice() throws NotSupportedException, SystemException {
		manager.begin();
		manager.begin();
	}

	@Test(expected=NotSupportedException.class)
	public void testBeginMarkedRollback() throws NotSupportedException, SystemException {
		manager.begin();
		manager.setRollbackOnly();
		manager.begin();
	}

	@Test
	public void testBeginAfterCommit() throws NotSupportedException, SystemException, SecurityException, IllegalStateException, RollbackException, HeuristicMixedException, HeuristicRollbackException {
		manager.begin();
		manager.commit();
		manager.begin();
	}

	@Test(expected=IllegalStateException.class)
	public void testCommitOnly() throws SecurityException, IllegalStateException, RollbackException, HeuristicMixedException, HeuristicRollbackException, SystemException {
		manager.commit();
	}

	@Test
	public void testTransactionCommit() throws SecurityException, IllegalStateException, RollbackException, HeuristicMixedException, HeuristicRollbackException, SystemException, NotSupportedException {
		manager.begin();
		manager.commit();
	}

	@Test(expected=IllegalStateException.class)
	public void testCommitTwice() throws SecurityException, IllegalStateException, RollbackException, HeuristicMixedException, HeuristicRollbackException, SystemException, NotSupportedException {
		manager.begin();
		manager.commit();
		manager.commit();
	}

	@Test
	public void testRollback() throws NotSupportedException, SystemException {
		manager.begin();
		manager.rollback();
	}


	@Test(expected=IllegalStateException.class)
	public void testRollbackOnly() throws SystemException {
		manager.rollback();
	}

	@Test
	public void testSetRollbackOnly() throws IllegalStateException, SystemException, NotSupportedException {
		manager.begin();
		manager.setRollbackOnly();
	}

	@Test(expected=IllegalStateException.class)
	public void testRollbackOnlyWithoutTransaction() throws IllegalStateException, SystemException {
		manager.setRollbackOnly();
	}

	@Test
	public void testGetStatus() throws SystemException {
		assertEquals(Status.STATUS_NO_TRANSACTION, manager.getStatus());
	}

	@Test
	public void testGetCommitStatus() throws SystemException, NotSupportedException, SecurityException, IllegalStateException, RollbackException, HeuristicMixedException, HeuristicRollbackException {
		manager.begin();
		manager.commit();
		assertEquals(Status.STATUS_COMMITTED, manager.getStatus());
	}

	@Test
	public void testGetRollbackStatus() throws SystemException, NotSupportedException {
		manager.begin();
		manager.rollback();
		assertEquals(Status.STATUS_ROLLEDBACK, manager.getStatus());
	}

	@Test
	public void testGetTransaction() throws SystemException {
		assertNull(manager.getTransaction());
	}

	@Test
	public void testRealGetTransaction() throws SystemException, NotSupportedException {
		manager.begin();
		assertNotNull(manager.getTransaction());
	}

	@Test
	public void testGetTransactionAfterCommit() throws SystemException, NotSupportedException, SecurityException, IllegalStateException, RollbackException, HeuristicMixedException, HeuristicRollbackException {
		manager.begin();
		manager.commit();
		assertNotNull(manager.getTransaction());
	}

	@Test
	public void testGetTransactionAfterRollback() throws SystemException, NotSupportedException {
		manager.begin();
		manager.rollback();
		assertNotNull(manager.getTransaction());
	}

	@Test
	public void testSetTransactionTimeout() throws SystemException {
		manager.setTransactionTimeout(34);
	}

	@Test
	public void testSetTransactionTimeoutInTransaction() throws SystemException, NotSupportedException {
		manager.begin();
		manager.setTransactionTimeout(34);
	}

	@Test
	public void testSetZeroTransactionTimeout() throws SystemException {
		manager.setTransactionTimeout(0);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testSetNegativeTransactionTimeout() throws SystemException {
		manager.setTransactionTimeout(-20);
	}

	@Test
	public void testSuspendWithoutTransaction() throws SystemException {
		assertNull(manager.suspend());
	}

	@Test
	public void testSuspend() throws SystemException, NotSupportedException {
		manager.begin();
		assertNotNull(manager.suspend());
		assertNull(manager.suspend());
	}

	@Test
	public void testResume() throws NotSupportedException, SystemException, InvalidTransactionException, IllegalStateException {
		manager.begin();
		Transaction tx;
		tx = manager.suspend();
		assertNull(manager.suspend());
		manager.resume(tx);
		assertNotNull(manager.suspend());
	}

	@Test(expected=IllegalStateException.class)
	public void testResumeOnActiveTransaction() throws NotSupportedException, SystemException, InvalidTransactionException, IllegalStateException {
		manager.begin();
		Transaction tx;
		tx = manager.suspend();
		manager.begin();
		manager.resume(tx);
	}

	@Test(expected=InvalidTransactionException.class)
	public void testResumeNull() throws NotSupportedException, SystemException, InvalidTransactionException, IllegalStateException {
		manager.begin();
		manager.suspend();
		manager.resume(null);
	}
}
