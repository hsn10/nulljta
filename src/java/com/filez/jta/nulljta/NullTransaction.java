/**
 *    Copyright 2013 Radim Kolar
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.filez.jta.nulljta;

import java.util.HashSet;
import java.util.Set;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.Synchronization;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.xa.XAResource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NullTransaction implements Transaction {

	private final static Logger LOG = LoggerFactory.getLogger(NullTransaction.class);

	/** is this transaction rollback only? */
	private boolean rollbackOnly;
	/** current transaction status */
	private int state;
	/** synchronization callbacks */
	private Set <Synchronization> callbacks = new HashSet <Synchronization> ();

	/** initialize transaction to initial values. Status will be set to ACTIVE */
	void init() {
		rollbackOnly = false;
		state = Status.STATUS_ACTIVE;
		callbacks.clear();
	}

	@Override
	public void commit() throws RollbackException, HeuristicMixedException,
	HeuristicRollbackException, SecurityException, SystemException {
		if (state != Status.STATUS_ACTIVE) {
			LOG.error("Can not commit not active transaction");
			throw new IllegalStateException("Can not commit not active transaction");
		}
		if (rollbackOnly == true) {
			state = Status.STATUS_ROLLEDBACK;
			rollbackOnly = false;
			LOG.info("Can not commit, transaction was marked rollback only");
			throw new RollbackException("Can not commit, transaction was marked rollback only");
		}
		// invoke callbacks
		for(Synchronization s:callbacks) {
			s.beforeCompletion();
		}
		state = Status.STATUS_COMMITTED;
		for(Synchronization s:callbacks) {
			s.afterCompletion(Status.STATUS_COMMITTED);
		}
		LOG.debug("commit executed");
	}

	@Override
	public void rollback() throws IllegalStateException, SystemException {
		if (state != Status.STATUS_ACTIVE) {
			LOG.error("Can not rollback not active transaction");
			throw new IllegalStateException("Can not rollback not active transaction");
		}

		// invoke callbacks
		state = Status.STATUS_ROLLEDBACK;
		for(Synchronization s:callbacks) {
			s.afterCompletion(Status.STATUS_ROLLEDBACK);
		}
		LOG.debug("rollback executed");
	}

	@Override
	public void setRollbackOnly() throws IllegalStateException, SystemException {
		if (state != Status.STATUS_ACTIVE) {
			LOG.error("Can not set rollbackOnly flag on not active transaction");
			throw new IllegalStateException("Can not set rollbackOnly flag on not active transaction");
		}
		rollbackOnly = true;
		LOG.debug("setRollbackOnly executed");
	}

	@Override
	public int getStatus() throws SystemException {
		if (state == Status.STATUS_ACTIVE && rollbackOnly == true) {
			LOG.debug("returning state MARKED_ROLLBACK");
			return Status.STATUS_MARKED_ROLLBACK;
		}
		LOG.debug("returning state {}", state);
		return state;
	}

	/** does nothing, allows any resource */
	@Override
	public boolean enlistResource(XAResource xaRes) throws RollbackException,
	IllegalStateException, SystemException {
		return true;
	}

	/** does nothing, allows any resource */
	@Override
	public boolean delistResource(XAResource xaRes, int flag)
			throws IllegalStateException, SystemException {
		return true;
	}

	@Override
	public void registerSynchronization(Synchronization sync)
			throws RollbackException, IllegalStateException, SystemException {
		if (sync == null) {
			LOG.error("You can not register null synchronization");
			throw new IllegalArgumentException("You can not register null synchronization");
		}
		if (rollbackOnly == true) {
			LOG.error("Can not register Synchronization for rollback only transaction");
			throw new RollbackException("Can not register Synchronization for rollback only transaction");
		}
		/* 
		 * while its not clear from JTA documentation if registering callback on
		 * transaction in state committed or rolled back should be allowed, we allow
		 * it, it seems that Spring JTA needs it.
		 */
		callbacks.add(sync);
		LOG.debug("Synchronization {} registered", sync);
		if (state != Status.STATUS_ACTIVE) {
			/* call after competition status immediately, SPRING seems to expect it */
			sync.afterCompletion(state);
			LOG.debug("afterCompletion sync called on newly registered sync");
		}
	}
}
