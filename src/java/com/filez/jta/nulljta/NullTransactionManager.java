/**
 *    Copyright 2013 Radim Kolar
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.filez.jta.nulljta;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.InvalidTransactionException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NullTransactionManager implements TransactionManager {

	private final static Logger LOG = LoggerFactory.getLogger(NullTransactionManager.class);

	private ThreadLocal <NullTransaction> state = new ThreadLocal<NullTransaction>();

	@Override
	public void begin() throws NotSupportedException, SystemException {
		NullTransaction ntx = state.get();
		if (ntx != null) {
			int status = ntx.getStatus();
			if (status == Status.STATUS_ACTIVE || status == Status.STATUS_MARKED_ROLLBACK) {
				LOG.error("Nested transactions are not supported");
				throw new NotSupportedException("Nested transactions are not supported");
			}
		} else
			ntx = new NullTransaction();
		ntx.init();
		state.set(ntx);
		LOG.debug("begin executed");
	}

	@Override
	public void commit() throws RollbackException, HeuristicMixedException,
	HeuristicRollbackException, SecurityException,
	IllegalStateException, SystemException {
		NullTransaction ntx = state.get();
		if (ntx == null) {
			LOG.error("Can not commit - no transaction associated with current thread");
			throw new IllegalStateException("Can not commit - no transaction associated with current thread");
		}
		ntx.commit();
	}

	@Override
	public void rollback() throws IllegalStateException, SecurityException,
	SystemException {
		NullTransaction ntx = state.get();
		if (ntx == null) {
			LOG.error("Can not rollback, no transaction associated with current thread");
			throw new IllegalStateException("Can not rollback, no transaction associated with current thread");
		}
		ntx.rollback();
	}

	@Override
	public void setRollbackOnly() throws IllegalStateException, SystemException {
		NullTransaction tx = state.get();
		if (tx == null) {
			LOG.error("Can not set rollBackOnly, no transaction associated with calling thread");
			throw new IllegalStateException("Can not set rollBackOnly, no transaction associated with calling thread");
		}
		tx.setRollbackOnly();
	}

	@Override
	public int getStatus() throws SystemException {
		NullTransaction tx = state.get();
		if (tx == null) {
			LOG.debug("returning state NO_TRANSACTION");
			return Status.STATUS_NO_TRANSACTION;
		}
		return tx.getStatus();
	}

	@Override
	public Transaction getTransaction() throws SystemException {
		NullTransaction tx = state.get();
		if (tx == null) {
			LOG.debug("no transaction associated with current thread, returning null from getTransaction");
			return null;
		}
		LOG.debug("returning thread transaction {}", tx);
		return tx;
	}

	@Override
	public void setTransactionTimeout(int seconds) throws SystemException {
		if (seconds < 0) {
			LOG.error("timeout must be positive or 0 for default value");
			throw new IllegalArgumentException("timeout must be positive or 0 for default value");
		}
	}

	@Override
	public Transaction suspend() throws SystemException {
		NullTransaction tx = state.get();
		state.set(null);
		LOG.debug("Suspend executed, returning {}", tx);
		return tx;
	}

	@Override
	public void resume(Transaction tobj) throws InvalidTransactionException,
	IllegalStateException, SystemException {
		if (state.get() != null) {
			LOG.error("Can not resume, there is already transaction associated with current thread");
			throw new IllegalStateException("Can not resume, there is already transaction associated with current thread");
		}
		if (tobj instanceof NullTransaction) {
			state.set((NullTransaction) tobj);
			LOG.debug("Resume executed with object {}", tobj);
		}
		else {
			LOG.error("Transaction object do not belongs to NullTransactionManager");
			throw new InvalidTransactionException("Transaction object do not belongs to NullTransactionManager");
		}
	}
}
