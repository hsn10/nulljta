/**
 *    Copyright 2013 Radim Kolar
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.filez.jta.nulljta;

import java.io.Serializable;

import javax.naming.NamingException;
import javax.naming.Reference;
import javax.naming.Referenceable;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

/** 
 * Null JTA user transaction implementation. All calls do nothing. 
 * Intended use is for hadoop spring batch job.
 * 
 * @author Radim Kolar
 */
public class NullUserTransaction implements UserTransaction, Serializable, Referenceable {

	private static final long serialVersionUID = 867806025705722919L;

	private transient NullTransactionManager manager;

	public NullUserTransaction(NullTransactionManager manager) {
		if (manager == null)
			throw new IllegalArgumentException("manager could not be null");
		this.manager = manager;
	}

	@Override
	public void begin() throws NotSupportedException, SystemException {
		manager.begin();
	}

	@Override
	public void commit() throws RollbackException, HeuristicMixedException,
	HeuristicRollbackException, SecurityException,
	IllegalStateException, SystemException {
		manager.commit();
	}

	@Override
	public void rollback() throws IllegalStateException, SecurityException,
	SystemException {
		manager.rollback();
	}

	@Override
	public void setRollbackOnly() throws IllegalStateException, SystemException {
		manager.setRollbackOnly();
	}

	@Override
	public int getStatus() throws SystemException {
		return manager.getStatus();
	}

	@Override
	public void setTransactionTimeout(int seconds) throws SystemException {
		manager.setTransactionTimeout(seconds);
	}

	@Override
	public Reference getReference() throws NamingException {
		return new Reference(NullUserTransaction.class.getName());
	}
}
